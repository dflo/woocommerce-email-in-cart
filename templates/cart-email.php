<?php
/**
 * Email Template on Cart Page
 *
 * @package Woocommerce_Add_Email_To_Cart_Page
 * @since   1.0.0
 */

	defined( 'ABSPATH' ) || exit;
	$customer_email = WC()->customer->get_billing_email();
?>

	<tr>
		<th colspan="2"><?php esc_html_e( 'Email', 'woocommerce-email-in-cart' ); ?><abbr class="required">*</abbr></th>
		<td colspan="3" data-title="<?php esc_attr_e( 'Email', 'woocommerce-email-in-cart' ); ?>" class="actions">
			<form class="" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
				<input type="email" class="input-text " name="weic_email_field" id="weic_email_field" placeholder= "<?php esc_html_e( 'Insert your Email', 'woocommerce-email-in-cart' ); ?>" value="<?php echo esc_html( $customer_email ); ?>" autocomplete="email username" required>
			</form>
		</td>
	</tr>

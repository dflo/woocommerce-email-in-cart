# Woocommerce Email In Cart
## Description
Plugin adds the field "Email" on the Cart page. User needs to add a valid email on the Cart page to move to Checkout. The field is saved as Customer Billing Address.

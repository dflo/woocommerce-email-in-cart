<?php
/**
 * Plugin Name: Woocommerce Email In Cart
 * Description: Adds Customer Email to the Woocommerce Cart Page
 * Version: 1.0.0
 * Author: Flora Dsouza
 * Author URI: https://profiles.wordpress.org/floradsouza/
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: woocommerce-email-in-cart
 *
 * @package Woocommerce_Add_Email_To_Cart_Page
 */

defined( 'ABSPATH' ) || exit;

// Define WEIC_PLUGIN_PATH.
if ( ! defined( 'WEIC_PLUGIN_PATH' ) ) {
	define( 'WEIC_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
}

// Define WEIC_PLUGIN_URL.
if ( ! defined( 'WEIC_PLUGIN_URL' ) ) {
	define( 'WEIC_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

require_once WEIC_PLUGIN_PATH . 'includes/class-woocommerce-email-in-cart.php';

Woocommerce_Email_In_Cart::instance();

<?php
/**
 * Adds, validates and saves the email field on the add-to-cart page.
 *
 * @package Woocommerce_Add_Email_To_Cart_Page
 * @since 1.0.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * This class adds, validates and saves the email field on the add-to-cart page.
 *
 * @since      1.0.0
 * @package    Woocommerce_Add_Email_To_Cart_Page
 * @subpackage Woocommerce_Add_Email_To_Cart_Page/includes
 */
class Woocommerce_Email_In_Cart {

	/**
	 * The single instance of the class
	 *
	 * @var Woocommerce_Email_In_Cart
	 * @since 1.0.0
	 */
	protected static $_instance = null;


	/**
	 * Main Woocommerce_Email_In_Cart Instance.
	 *
	 * Ensures only one instance of Woocommerce_Email_In_Cart is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @return Woocommerce_Email_In_Cart Main instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Initialize the class.
	 */
	public function __construct() {
		add_action( 'woocommerce_after_cart_contents', array( $this, 'add_email_field' ), 1 );
		add_action( 'wp_ajax_save_email_ajax', array( $this, 'save_email_ajax' ) );
		add_action( 'wp_ajax_nopriv_save_email_ajax', array( $this, 'save_email_ajax' ) );
	}

	/**
	 * Adds the email field to the cart page.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function add_email_field() {
		if ( is_cart() ) {
			wp_enqueue_script( 'weic-email-validator', WEIC_PLUGIN_URL . 'assets/js/email-field-validate.js', array(), '1.0.0', true );

			wp_enqueue_style( 'weic-email-field', WEIC_PLUGIN_URL . 'assets/css/email-field.css', array(), '1.0.0', 'all' );

			wp_localize_script(
				'weic-email-validator',
				'email_val',
				array(
					'valid_email' => __( 'Please enter a valid email address.', 'woocommerce-email-in-cart' ),
					'ajax_url'    => admin_url( 'admin-ajax.php' ),
					'cart_url'    => wc_get_cart_url(),
					'email_nonce' => wp_create_nonce( 'weic-email-nonce' ),
				)
			);

			wc_get_template(
				'cart-email.php',
				array(),
				'',
				WEIC_PLUGIN_PATH . 'templates/'
			);
		}
	}

	/**
	 * Ajax to save the email field added to the cart page.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function save_email_ajax() {
		$response = array();

		try {
			$nonce_value = wc_get_var( $_POST['email_nonce'], '' ); // @codingStandardsIgnoreLine.

			if ( ! wp_verify_nonce( $nonce_value, 'weic-email-nonce' ) ) {
				$response['message'] = __( 'Authentication failed.', 'woocommerce-email-in-cart' );
				wp_send_json_error( $response );
			}

			if ( isset( $_POST['weic_email_field'] ) && ( ! empty( $_POST['weic_email_field'] ) ) ) {
				if ( is_email( $_POST['weic_email_field'] ) ) {
					WC()->customer->set_billing_email( $_POST['weic_email_field'] );
				} else {
					$response['message'] = __( 'Please enter a valid email.', 'woocommerce-email-in-cart' );
					wp_send_json_error( $response );
				}
			} else {
				$customer_email = WC()->customer->get_billing_email();

				if ( ( ! $customer_email ) || empty( $customer_email ) ) {
					$response['message'] = __( 'Please enter a valid email.', 'woocommerce-email-in-cart' );
					wp_send_json_error( $response );
				}
			}
			wp_send_json_success( $response );
		} catch ( Exception $e ) {
			$response['message'] = $e->getMessage();
			wp_send_json_error( $response );
		}
	}
}

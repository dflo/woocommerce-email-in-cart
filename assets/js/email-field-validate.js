jQuery( document ).ready(
	function($) {
		$( '.checkout-button' ).click(
			function(e) {
				e.preventDefault();
				var checkoutURL = jQuery( this ).attr( "href" );

				var emailField = $( '#weic_email_field' ).val();

				$.ajax(
					{
						url: email_val.ajax_url,
						type: 'post',
						dataType: 'json',
						data:  {
							'action': 'save_email_ajax',
							'weic_email_field': emailField,
							'email_nonce': email_val.email_nonce,
						},
						success: function (response) {
							if (response.success) {
								window.location.href = checkoutURL;
							} else {
								$( '.woocommerce-error' ).remove();
								jQuery( '.woocommerce-notices-wrapper' ).empty();
								jQuery( '.woocommerce-notices-wrapper' ).append(
									'<ul class="woocommerce-error weic-error" role="alert"><li>' +
									response.data.message + '</li></ul>'
								);
								document.querySelector( '.woocommerce-notices-wrapper' ).scrollIntoView( true );
							}
						}
						}
				);
			}
		);

		function validateEmail(mail) {
			if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test( mail )) {
				jQuery( '.weic-error' ).remove();
				return (true)
			}
			jQuery( '.woocommerce-notices-wrapper' ).empty();
			jQuery( '.woocommerce-notices-wrapper' ).append( '<ul class="woocommerce-error weic-error" role="alert"><li>Please enter a valid email.</li></ul>' );
			document.querySelector( '.woocommerce-notices-wrapper' ).scrollIntoView( true );
			return (false)
		}

		jQuery( '#weic_email_field' ).focusout(
			function(){
				var val = jQuery( this ).val();
				validateEmail( val );
			}
		)
	}
)
